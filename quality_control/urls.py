from django.urls import path, include
from quality_control import views

app_name = 'quality_control'

urlpatterns = [
    path('', views.index, name = 'index'),
    path('bug/', views.bug_list, name='bug_list'),  # новый маршрут
    path('bug/<int:bugreport_id>/', views.bugreport_detail, name='bugreport_detail'),
    path('feature/', views.feature_list, name='feature_list'),
    path('feature/<int:featurerequest_id>/', views.featurerequest_detail, name='featurerequest_detail'),
    path('bug/new/', views.create_bugreport, name='create_bugreport'),
    path('feature/new/', views.create_featurerequest, name='create_featurerequest'),
    path('bug/<int:bugreport_id>/update/', views.update_bugreport, name='update_bugreport'),
    path('feature/<int:featurerequest_id>/update/', views.update_featurerequest, name='update_featurerequest'),
    path('bug/<int:bugreport_id>/delete/', views.delete_bugreport, name='delete_bugreport'),
    path('feature/<int:featurerequest_id>/delete/', views.delete_featurerequest, name='delete_featurerequest'),
    # path('', views.IndexView.as_view(), name='index'),
    # path('bug/', views.BugListView.as_view(), name='bug_list'),
    # path('bug/<int:bugreport_id>/', views.BugDetailView.as_view(), name='bugreport_detail'),
    # path('feature/', views.FeatureListView.as_view(), name='feature_list'),
    # path('feature/<int:featurerequest_id>/', views.FeatureDetailView.as_view(), name='featurerequest_detail'),
    # path('bug/<int:bugreport_id>/delete/', views.BugReportDeleteView.as_view(), name='delete_bugreport'),
    # path('feature/<int:featurerequest_id>/delete/', views.FeatureRequestDeleteView.as_view(), name='delete_featurerequest'),
    # path('bug/<int:bugreport_id>/update/', views.BugReportUpdateView.as_view(), name='update_bugreport'),
    # path('feature/<int:featurerequest_id>/update/', views.FeatureRequestUpdateView.as_view(), name='update_featurerequest'),
    # path('bug/new/', views.BugReportCreateView.as_view(), name='create_bugreport'),
    # path('feature/new/', views.FeatureRequestCreateView.as_view(), name='create_featurerequest'),
]