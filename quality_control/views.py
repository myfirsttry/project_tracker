from django.http import HttpResponse
from django.urls import reverse
from django.shortcuts import get_object_or_404
from .models import FeatureRequest, BugReport
from django.shortcuts import render
from django.shortcuts import render, redirect
from .forms import BugReportForm
from .forms import FeatureRequestForm
from django.views.generic import View
from django.views.generic import ListView
from django.views.generic import DetailView






def index(request):
    return render(request, 'quality_control/index.html')

def bug_list(request):
    bugs = BugReport.objects.all()
    return render(request, 'quality_control/bug_list.html', {'bug_list': bugs})



def bugreport_detail(request, bugreport_id):
    bug = get_object_or_404(BugReport, id=bugreport_id)
    return render(request, 'quality_control/bugreport_detail.html', {'bugreport_detail': bug})

def feature_list(request):
    features = FeatureRequest.objects.all()
    return render(request, 'quality_control/feature_list.html', {'feature_list': features})

def featurerequest_detail(request, featurerequest_id):
    feature = get_object_or_404(FeatureRequest, id=featurerequest_id)
    return render(request, 'quality_control/featurerequest_detail.html', {'featurerequest_detail': feature})


def create_bugreport(request):
    if request.method == 'POST':
        form = BugReportForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('quality_control:bug_list')
    else:
        form = BugReportForm()
    return render(request, 'quality_control/bug_report_form.html', {'form': form})

from .forms import FeatureRequestForm

def create_featurerequest(request):
    if request.method == 'POST':
        form = FeatureRequestForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('quality_control:feature_list')
    else:
        form = FeatureRequestForm()
    return render(request, 'quality_control/feature_request_form.html', {'form': form})




def update_bugreport(request, bugreport_id):
    bugreport = get_object_or_404(BugReport, pk=bugreport_id)
    if request.method == 'POST':
        form = BugReportForm(request.POST, instance=bugreport)
        if form.is_valid():
            form.save()
            return redirect('quality_control:bugreport_detail', bugreport_id=bugreport.id)
    else:
        form = BugReportForm(instance=bugreport)
    return render(request, 'quality_control/bugreport_update.html', {'form': form, 'bugreport': bugreport})

def update_featurerequest(request, featurerequest_id):
    featurerequest = get_object_or_404(FeatureRequest, pk=featurerequest_id)
    if request.method == 'POST':
        form = FeatureRequestForm(request.POST, instance=featurerequest)
        if form.is_valid():
            form.save()
            return redirect('quality_control:featurerequest_detail', featurerequest_id=featurerequest.id)
    else:
        form = FeatureRequestForm(instance=featurerequest)
    return render(request, 'quality_control/featurerequest_update.html', {'form': form, 'featurerequest': featurerequest})

def delete_bugreport(request, bugreport_id):
    bugreport = get_object_or_404(BugReport, pk=bugreport_id)
    bugreport.delete()
    return redirect('quality_control:bug_list')


def delete_featurerequest(request, featurerequest_id):
    featurerequest = get_object_or_404(FeatureRequest, pk=featurerequest_id)
    featurerequest.delete()
    return redirect('quality_control:feature_list')




# class IndexView(View):
#     def get(self, request, *args, **kwargs):
#         return render(request, 'quality_control/index.html')


# class BugListView(ListView):
#     model = BugReport
#     template_name = 'quality_control/bug_list.html'
    
# class BugDetailView(DetailView):
#     model = BugReport
#     pk_url_kwarg = 'bugreport_id'
#     template_name = 'quality_control/bugreport_detail.html'
    
# class FeatureListView(ListView):
#     model = FeatureRequest
#     template_name = 'quality_control/feature_list.html'
    
# class FeatureDetailView(DetailView):
#     model = FeatureRequest
#     pk_url_kwarg = 'featurerequest_id'
#     template_name = 'quality_control/featurerequest_detail.html'

# from django.views.generic import CreateView
# from django.urls import reverse, reverse_lazy

# class BugReportCreateView(CreateView):
#     model = BugReport
#     form_class = BugReportForm
#     template_name = 'quality_control/bug_report_form.html'
#     success_url = reverse_lazy('quality_control:bug_list')
    
# class FeatureRequestCreateView(CreateView):
#     model = FeatureRequest
#     form_class = FeatureRequestForm
#     template_name = 'quality_control/featurerequest_create.html'
#     success_url = reverse_lazy('quality_control:feature_list')

# from django.views.generic.edit import UpdateView
# from django.urls import reverse_lazy
# from .models import Project
# from .forms import BugReportForm
# from .forms import FeatureRequestForm

# class BugReportUpdateView(UpdateView):
#     model = BugReport
#     form_class = BugReportForm
#     template_name = 'tasks/bugreport_update.html'
#     pk_url_kwarg = 'bugreport_id'
#     success_url = reverse_lazy('quality_control:bug_list')
    
# class FeatureRequestUpdateView(UpdateView):
#     model = FeatureRequest
#     form_class = FeatureRequestForm
#     template_name = 'tasks/featurerequest_update.html'
#     pk_url_kwarg = 'featurerequest_id'
#     success_url = reverse_lazy('quality_control:feature_list')

# from django.views.generic.edit import DeleteView

# class BugReportDeleteView(DeleteView):
#     model = BugReport
#     pk_url_kwarg = 'bugreport_id'
#     success_url = reverse_lazy('quality_control:bug_list')
#     template_name = 'quality_control/bugreport_confirm_delete.html'

# class FeatureRequestDeleteView(DeleteView):
#     model = FeatureRequest
#     pk_url_kwarg = 'featurerequest_id'
#     success_url = reverse_lazy('quality_control:feature_list')
#     template_name = 'quality_control/featurerequest_confirm_delete.html'

























# def index(request):
#     bugreports_list_url = reverse('quality_control:bug_list')
#     featurerequests_page_url = reverse('quality_control:feature_list')
#     html = f"<h1>Система контроля качества</h1><a href='{bugreports_list_url}'>Список всех багов</a><a href='{featurerequests_page_url}'>Запросы на улучшение</a>"
#     return HttpResponse(html)


# def bug_list(request):
#     bugreports = BugReport.objects.all()
#     bugreports_html = '<h1>Список багов</h1><ul>'
#     for bugreport in bugreports:
#         bugreports_html += f'<li><a href="{bugreport.id}/">{bugreport.title}</a>-{bugreport.status}</a></li>'
#     bugreports_html += '</ul>'
#     return HttpResponse(bugreports_html)

# def feature_list(request):
#     featurerequests = FeatureRequest.objects.all()
#     featurerequests_html = '<h1>Список гениальных идей</h1><ul>'
#     for featurerequest in featurerequests:
#         featurerequests_html += f'<li><a href="{featurerequest.id}/">{featurerequest.title}</a>>{featurerequest.status}</a></li>'
#     featurerequests_html += '</ul>'
#     return HttpResponse(featurerequests_html)

# def bugreport_detail(request, bugreport_id):
#     bugreport = get_object_or_404(BugReport, id=bugreport_id)
#     response_html = f'<h1>{bugreport.title}</h1><p>{bugreport.description}</p>'
#     return HttpResponse(response_html)

# def featurerequest_detail(request, featurerequest_id):
#     featurerequest = get_object_or_404(FeatureRequest, id=featurerequest_id)
#     response_html = f'<h1>{featurerequest.title}</h1><p>{featurerequest.description}</p>'
#     return HttpResponse(response_html)

# from django.views import View

# class IndexView(View):
#     def get(self, request, *args, **kwargs):
#         bugreports_list_url = reverse('quality_control:bug_list')
#         featurerequests_page_url = reverse('quality_control:feature_list')
#         html = f"<h1>Система контроля качества</h1><a href='{bugreports_list_url}'>Список всех багов</p><a href='{featurerequests_page_url}'>Запросы на улучшение</a>"
#         return HttpResponse(html)
    
# from django.views.generic import ListView

# class BugListView(ListView):
#     model = BugReport

#     def get(self, request, *args, **kwargs):
#         bugs = self.get_queryset()
#         bugs_html = '<h1>Список багов</h1><ul>'
#         for bugreport in bugs:
#             bugs_html += f'<li><a href="{bugreport.id}/">{bugreport.title}</a></li>'
#         bugs_html += '</ul>'
#         return HttpResponse(bugs_html)

# class FeatureListView(ListView):
#     model = FeatureRequest

#     def get(self, request, *args, **kwargs):
#         features = self.get_queryset()
#         features_html = '<h1>Список гениальных идей</h1><ul>'
#         for featurerequest in features:
#             features_html += f'<li><a href="{featurerequest.id}/">{featurerequest.title}</a></li>'
#         features_html += '</ul>'
#         return HttpResponse(features_html)

# from django.views.generic import DetailView

# class BugDetailView(DetailView):
#     model = BugReport
#     pk_url_kwarg = 'bugreport_id'

#     def get(self, request, *args, **kwargs):
#         bugreports = self.get_object()
#         response_html = f'<h1>{bugreports.title}</h1><p>{bugreports.description}</p><p>{bugreports.status}</p><p>{bugreports.priority}</p><p>{bugreports.task}</p><p>{bugreports.project}</p>'
#         return HttpResponse(response_html)
    
# class FeatureDetailView(DetailView):
#     model = FeatureRequest
#     pk_url_kwarg = 'featurerequest_id'

#     def get(self, request, *args, **kwargs):
#         featurerequests = self.get_object()
#         response_html = f'<h1>{featurerequests.title}</h1><p>{featurerequests.description}</p><p>{featurerequests.status}</p><p>{featurerequests.priority}</p><p>{featurerequests.task}</p><p>{featurerequests.project}</p>'
#         return HttpResponse(response_html)