from django.forms import ModelForm
from .models import BugReport

class BugReportForm(ModelForm):
    class Meta:
        model = BugReport
        fields = ['title', 'description','status','project','task','priority']

from .models import FeatureRequest

class FeatureRequestForm(ModelForm):
    class Meta:
        model = FeatureRequest
        fields = ['title', 'description','status','project','task','priority']
