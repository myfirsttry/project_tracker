from django.db import models
from tasks.models import Project
from tasks.models import Task

class BugReport(models.Model):
    STATUS_CHOICES = [
        ('New', 'Новая'),
        ('In_progress', 'В работе'),
        ('Completed', 'Завершена'),
    ]
    PRIORITY_CHOICES = [
        ('New', 'Важно'),
        ('In_progress', 'Можно отложить'),
        ('Completed', 'Надо сделать'),
    ]
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=100)
    description = models.TextField()
    project = models.ForeignKey(
        Project,
        related_name='bugreport',
        on_delete=models.CASCADE
    )
    task = models.ForeignKey(
        Task,
        related_name='bugreport',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    
    
    priority = models.CharField(
        max_length=50,
        choices=PRIORITY_CHOICES,
        default='Важно',
    )
    status = models.CharField(
        max_length=50,
        choices=STATUS_CHOICES,
        default='New',
    )
class FeatureRequest(models.Model):
    STATUS_CHOICES = [
        ('New', 'Новая'),
        ('In_progress', 'В работе'),
        ('Completed', 'Завершена'),
    ]
    PRIORITY_CHOICES = [
        ('New', 'Важно'),
        ('In_progress', 'Можно отложить'),
        ('Completed', 'Надо сделать'),
    ]
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=100)
    description = models.TextField()
    project = models.ForeignKey(
        Project,
        related_name='featurerequest',
        on_delete=models.CASCADE
    )
    task = models.ForeignKey(
        Task,
        related_name='featurerequest',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    
    
    priority = models.CharField(
        max_length=50,
        choices=PRIORITY_CHOICES,
        default='Важно',
    )
    status = models.CharField(
        max_length=50,
        choices=STATUS_CHOICES,
        default='New',
    )